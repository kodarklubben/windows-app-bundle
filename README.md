# Kodarklubben Windows app bundle

Requirements:

- NSIS
- A lot of packages

### Instructions

1. Download all the required installers to the packages directory.
2. Run `makensis.exe` or `makensisw.exe` and compile the installer.

### Todo

- Downloader task
