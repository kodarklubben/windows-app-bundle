;Kodarklubben Windows Software Bundle
;
;Author: Jan Lindblom <jan.lindblom@kodarklubben.ax>

!include Sections.nsh

!define PRODUCT_NAME "Kodarklubben Windows Bundle"
!define PRODUCT_VERSION "1.0.0"
!define PRODUCT_PUBLISHER "Kodarklubben"

SetCompress off
SetCompressor lzma
RequestExecutionLevel admin

; Modern UI
!include "MUI2.nsh"

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_COMPONENTS
;!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
;Var StartMenuFolder
;!insertmacro MUI_PAGE_STARTMENU "Kodarklubben" $StartMenuFolder
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

!insertmacro MUI_LANGUAGE "English"

Name "Kodarklubben Windows Bundle"
OutFile "kodarklubben_setup.exe"

InstallDir "$PROGRAMFILES64\Kodarklubben"
InstallDirRegKey HKCU "Software\Kodarklubben" ""

ShowInstDetails show

!define MUI_ABORTWARNING

;!define JAVA8_JRE_LINK "http://download.oracle.com/otn-pub/java/jdk/8u181-b13/96a7b8442fe848ef90c96a2fad6ed6d1/jre-8u181-windows-x64.exe"
;!define JAVA8_JDK_LINK "http://download.oracle.com/otn-pub/java/jdk/8u181-b13/96a7b8442fe848ef90c96a2fad6ed6d1/jdk-8u181-windows-x64.exe"
;!define JAVA10_JRE_LINK "http://download.oracle.com/otn-pub/java/jdk/10.0.2+13/19aef61b38124481863b1413dce1855f/jre-10.0.2_windows-x64_bin.exe"
;!define JAVA10_JDK_LINK "http://download.oracle.com/otn-pub/java/jdk/10.0.2+13/19aef61b38124481863b1413dce1855f/jdk-10.0.2_windows-x64_bin.exe"


;!define JAVA8_FLAGS "/s AUTO_UPDATE=1 OEMUPDATE=0 WEB_JAVA_SECURITY_LEVEL=H INSTALL_SILENT=1 STATIC=0 REBOOT=0 SPONSORS=0 EULA=0 WEB_ANALYTICS=0"
;!define JAVA10_JDK_FLAGS "/s"
;!define JAVA10_JRE_FLAGS "INSTALL_SILENT=1 AUTO_UPDATE=1 REBOOT=0 SPONSORS=0"

Section -SETTINGS
    SetOutPath $INSTDIR
    SetOverwrite ifnewer
SectionEnd

SectionGroup /e "Requirements" Sec001Req
    Section "Adobe AIR" Sec01AdobeAir
        SectionIn RO
        File "packages\AdobeAIRInstaller.exe"
        ExecWait "$INSTDIR\AdobeAIRInstaller.exe -silent -eulaAccepted"
    SectionEnd
    Section "Java 8" Sec02Java08
        SectionIn RO
        File "packages\jdk-8u181-windows-i586.exe"
        AddSize 207000
        File "packages\jdk-8u181-windows-x64.exe"
        AddSize 207000

        ExecWait "$INSTDIR\jdk-8u181-windows-i586.exe INSTALL_SILENT=1 AUTO_UPDATE=1 REBOOT=0 SPONSORS=0 REMOVEOUTOFDATEJRES=1"
        ExecWait "$INSTDIR\jdk-8u181-windows-x64.exe INSTALL_SILENT=1 AUTO_UPDATE=1 REBOOT=0 SPONSORS=0 REMOVEOUTOFDATEJRES=1"
    SectionEnd
    Section "Java 10" Sec02Java10
        File "packages\jdk-10.0.2_windows-x64_bin.exe"
        AddSize 483000
        ExecWait "$INSTDIR\jdk-10.0.2_windows-x64_bin.exe /s INSTALL_SILENT=1 AUTO_UPDATE=1 REBOOT=0 SPONSORS=0 REMOVEOUTOFDATEJRES=1"
    SectionEnd
    Section "Java 11" Sec02Java11
        File "packages\jdk-11_windows-x64_bin.exe"
        AddSize 483000
        ExecWait "$INSTDIR\jdk-11_windows-x64_bin.exe /s INSTALL_SILENT=1 AUTO_UPDATE=1 REBOOT=0 SPONSORS=0 REMOVEOUTOFDATEJRES=1"
    SectionEnd
SectionGroupEnd

Section /o "Finch Robot Packages" Sec03Finch
    File "packages\BirdBrainRobotServerInstaller.msi"
    File "packages\CREATELabVisualProgrammerForFinch.msi"
    ExecWait '"msiexec" /i "$INSTDIR\BirdBrainRobotServerInstaller.msi" /quiet /norestart'
    ExecWait '"msiexec" /i "$INSTDIR\CREATELabVisualProgrammerForFinch.msi" /quiet /norestart'
SectionEnd

SectionGroup /e "Browsers" Sec02Browsers
  Section /o "Firefox" Sec02Firefox
    File "packages\Firefox Setup 62.0.2.exe"
    ExecWait "$INSTDIR\Firefox Setup 62.0.2.exe /s"
  SectionEnd
  Section /o "Google Chrome" Sec02Chrome
    File "packages\ChromeSetup.exe"
    ExecWait "$INSTDIR\ChromeSetup.exe /silent /install"
  SectionEnd
SectionGroupEnd

Section /o "Scratch" Sec03Scratch
    File "packages\Scratch-461.exe"
    ExecWait "$INSTDIR\Scratch-461.exe -silent"
SectionEnd

Section /o "Unity" Sec04Unity
    File "packages\UnitySetup64-2018.1.6f1.exe"
    File "packages\UnityStandardAssetsSetup-2018.1.6f1.exe"
    ExecWait "$INSTDIR\UnitySetup64-2018.1.6f1.exe /S /AllUsers"
    ExecWait "$INSTDIR\UnityStandardAssetsSetup-2018.1.6f1.exe /S /AllUsers"
SectionEnd

Section -INSTALLER
    WriteRegStr HKCU "Software\Kodarklubben" "" $INSTDIR
    WriteUninstaller "$INSTDIR\Uninstall.exe"
SectionEnd

Section "Uninstall"
    Delete /REBOOTOK "$INSTDIR\jdk-8u181-windows-i586.exe"
    Delete /REBOOTOK "$INSTDIR\jdk-8u181-windows-x64.exe"
    Delete "$INSTDIR\Uninstall.exe"
    RMDir "$INSTDIR"
    DeleteRegKey /ifempty HKCU "Software\Kodarklubben"
SectionEnd
